## About

A CHICKEN binding to the [libui] library.  Covers all parts of the
Alpha 3.1 API to make the examples run.  Additionally to that, there's
a high-level SXML interface for creating GUIs in a declarative manner.

## Docs

See [its wiki page].

[libui]: https://github.com/andlabs/libui
[its wiki page]: http://wiki.call-cc.org/eggref/4/libui
